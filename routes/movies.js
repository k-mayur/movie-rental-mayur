
const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const {ensureAuthenticated} = require('../helpers/auth');


// load movie model
require('../models/Movies');
const Movie = mongoose.model('movies');

// movie index page
router.get('/', ensureAuthenticated, (req, res) => {
    Movie.find()
        .sort({title : 'asc'})
        .then(movies => {
            res.render('movies/index', {
                movies : movies
            });
        });    
});

// add movie form
router.get('/add', ensureAuthenticated, (req, res) => {
    res.render('movies/add');
});

// edit movie form
router.get('/edit/:id', ensureAuthenticated, (req, res) => {
    Movie.findOne({
        _id : req.params.id
    }).then(movie => {
        res.render('movies/edit', {
            movie : movie
        });
    })  
});

// add process form
router.post('/', ensureAuthenticated, (req, res) => {    
    const newMovie = {
        title : req.body.title,
        genre : req.body.genre,
        numberInStock : req.body.stock,
        dailyRentalRate : req.body.rent
    };
    new Movie(newMovie)
        .save()
        .then(movie => {
            req.flash('success_msg', 'Movie added.')
            res.redirect('/api/movies');
        });    
});

// go to movie page
router.get('/:id', ensureAuthenticated, (req, res) => {
    Movie.findOne({
        _id : req.params.id
    })
    .then(movie => {
        res.render('movies/detail', {
            movie : movie
        });
    });    
});

// edit movie process
router.put('/edit/:id', ensureAuthenticated, (req, res) => {
    Movie.findOne({
        _id : req.params.id
    })
    .then(movie => {
        // new values
        movie.title = req.body.title;
        movie.genre = req.body.genre;
        movie.numberInStock = req.body.stock;
        movie.dailyRentalRate = req.body.rent;

        movie.save()
            .then(movie => {
                req.flash('success_msg', 'Movie Updated.')
                res.redirect('/api/movies');
            })
    });
});

// delete movie
router.delete('/:id', ensureAuthenticated, (req, res) => {
    Movie.deleteOne({_id : req.params.id})
        .then(() => {
            req.flash('success_msg', 'Movie removed.')
            res.redirect('/api/movies');
        })
});

module.exports = router;