
const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const {ensureAuthenticated} = require('../helpers/auth');


// load movie and rental model
require('../models/Rental');
require('../models/Movies')
const Rental = mongoose.model('rentals');
const Movie = mongoose.model('movies');

// movie index page
router.get('/', ensureAuthenticated, (req, res) => {
    Rental.find({user : req.user.id})
        .sort({dateIssued : 'asc'})
        .then(rentals => {
            newr = [];
            rentals.forEach(rental => {
                newr.push({
                    user : rental.user,
                    movie : rental.movie,
                    rentalFee : rental.rentalFee,
                    dateIssued : rental.dateIssued.toString().substring(0, 15),
                    dateReturned : rental.dateReturned.toString().substring(0, 15)
                })
                return newr;
            })
            res.render('rentals/index', {
                rentals : newr
            });
        });    
});

//date diff function
const _MS_PER_DAY = 1000 * 60 * 60 * 24;
function dateDiffInDays(date1, date2) {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    } 
    if (mm < 10) {
        mm = '0' + mm;
    } 
    today = `${yyyy}-${mm}-${dd}`;
    const a = new Date(date1);
    const b = new Date(date2);
    const c = new Date(today);
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    const utc3 = Date.UTC(c.getFullYear(), c.getMonth(), c.getDate());

    if (utc1 - utc3 < 0) {
        return false;
    } else if (utc2 - utc1 < 0) {
        return false;
    } else {
        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
}

// add process form
router.post('/', ensureAuthenticated, (req, res) => {
    const issueDate = req.body.dateIssued;
    const returnDate = req.body.dateReturned;
    const diff = dateDiffInDays(issueDate, returnDate);

    if (diff === false) {
        req.flash('error_msg', 'Please Enter Valid Dates');
        res.redirect(`/api/movies/${req.body.hidden}`);
    } else {
        Movie.findOne({_id : req.body.hidden})
        .then(movie => {
            const rent = diff * movie.dailyRentalRate
            const newRental = {
                user : req.user.id,
                movie : movie.title,
                dateIssued : issueDate,
                dateReturned : returnDate,
                rentalFee : rent
            };
            new Rental(newRental)
                .save()
                .then(rental => {
                    req.flash('success_msg', 'Rental added.')
                    res.redirect('/api/rentals');
                });
        })
    }        
});



module.exports = router;