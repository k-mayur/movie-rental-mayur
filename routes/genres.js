const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const {ensureAuthenticated} = require('../helpers/auth');


// load genre and movie model
require('../models/Genre');
require('../models/Movies');
const Genre = mongoose.model('genres');
const Movie = mongoose.model('movies');

// genre index page
router.get('/', ensureAuthenticated, (req, res) => {
    Genre.find()
        .sort({title : 'asc'})
        .then(genres => {
            res.render('genres/index', {
                genres : genres
            });
        });    
});

// add genre form
router.get('/add', ensureAuthenticated, (req, res) => {
    res.render('genres/add');
});

// edit genre form
router.get('/edit/:id', ensureAuthenticated, (req, res) => {
    Genre.findOne({
        _id : req.params.id
    }).then(genre => {
        res.render('genres/edit', {
            genre : genre
        });
    })  
});

// add process form
router.post('/', ensureAuthenticated, (req, res) => {    
    const newGenre = {
        title : req.body.title
    };
    new Genre(newGenre)
        .save()
        .then(genre => {
            req.flash('success_msg', 'Genre added.')
            res.redirect('/api/genres');
        });    
});

// go to genre page
router.get('/:id', ensureAuthenticated, (req, res) => {
    Genre.findOne({
        _id : req.params.id
    }).then(genre => {
        Movie.find({
            genre : genre.title
        })
        .then(movie => {
            res.render('genres/detail', {
                movies : movie
            });
        });  
    })      
});

// edit genre process
router.put('/edit/:id', ensureAuthenticated, (req, res) => {
    Genre.findOne({
        _id : req.params.id
    })
    .then(genre => {
        // new values
        genre.title = req.body.title;
        genre.save()
            .then(genre => {
                req.flash('success_msg', 'Genre Updated.')
                res.redirect('/api/genres');
            })
    });
});

// delete genre
router.delete('/:id', ensureAuthenticated, (req, res) => {
    Genre.deleteOne({_id : req.params.id})
        .then(() => {
            req.flash('success_msg', 'Genre removed.')
            res.redirect('/api/genres');
        })
});

module.exports = router;