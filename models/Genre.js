const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const GenreSchema = new Schema({
    title: {
        type: String,
        required: true
    }
});

mongoose.model("genres", GenreSchema);