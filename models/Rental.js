const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const RentalSchema = new Schema({
    user: {
        type: String,
        required: true
    },
    movie: {
        type: String,
        required: true
    },
    dateIssued: {
        type: Date,
        required: true
    },
    dateReturned: {
        type: Date,
        required: true
    },
    rentalFee : {
        type : Number,
        required : true
    }
});

mongoose.model("rentals", RentalSchema);