const express = require('express');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');
const app = express();

// load routes
const movies = require('./routes/movies');
const users = require('./routes/users');
const genres = require('./routes/genres');
const rentals = require('./routes/rentals');

// passport config
require('./config/passport')(passport);

// connect to mongoose
mongoose.connect('mongodb://localhost/movie-rental', {
    useNewUrlParser : true
})
.then(() => console.log('MongoDb connected'))
.catch(err => console.log(err));


// local stylesheet
app.use(express.static(__dirname + '/public'));


// handlebars middleware
app.engine('handlebars', exphbs({
    defaultLayout : 'main'
}));
app.set('view engine', 'handlebars');

// body-parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// method override middleware 
app.use(methodOverride('_method'));

// express session middleware
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

// passport middleware must be put after express session middleware
app.use(passport.initialize());
app.use(passport.session());


// flash middleware
app.use(flash());
// global variables for flash
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

// handle browser back button cache
app.use((req, res, next) => {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});


// index route
app.get('/', (req, res) => {
    const title = 'Rent Movies';
    res.render('index', {
        title1 : title
    });
});

// about route
app.get('/api/about', (req, res) => {
    res.render('about');
});

// use routes
app.use('/api/movies', movies);
app.use('/api/users', users);
app.use('/api/genres', genres);
app.use('/api/rentals', rentals);




const port = 3000;
app.listen(port, () => {
    console.log(`server started on port ${port}`);
});
